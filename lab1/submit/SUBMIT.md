
## Submission

Please answer the following questions in Markdown. Push your code to
BitBucket to submit this assignment.

1) The following shows the `ifconfig` command and its output for the client and server in *my* experiment. Replace these blocks with the `ifconfig` output of the client and server from *your* experiment.

```
ffund@client:~$ sudo ifconfig
eth0      Link encap:Ethernet  HWaddr 02:28:58:44:a6:67  
          inet addr:172.17.3.18  Bcast:172.31.255.255  Mask:255.240.0.0
          inet6 addr: fe80::28:58ff:fe44:a667/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:2054 errors:0 dropped:0 overruns:0 frame:0
          TX packets:2102 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:141795 (141.7 KB)  TX bytes:172055 (172.0 KB)
          Interrupt:25

eth1      Link encap:Ethernet  HWaddr 02:87:85:f5:d7:86  
          inet addr:10.1.1.1  Bcast:10.1.1.255  Mask:255.255.255.0
          inet6 addr: fe80::87:85ff:fef5:d786/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:114 errors:0 dropped:0 overruns:0 frame:0
          TX packets:55 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:6436 (6.4 KB)  TX bytes:4918 (4.9 KB)
          Interrupt:26

lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:16436  Metric:1
          RX packets:4 errors:0 dropped:0 overruns:0 frame:0
          TX packets:4 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:260 (260.0 B)  TX bytes:260 (260.0 B)
```

```
ffund@server:~$ sudo ifconfig
eth0      Link encap:Ethernet  HWaddr 02:e4:f6:be:2d:a3  
          inet addr:172.17.3.19  Bcast:172.31.255.255  Mask:255.240.0.0
          inet6 addr: fe80::e4:f6ff:febe:2da3/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:1946 errors:0 dropped:0 overruns:0 frame:0
          TX packets:1969 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:132935 (132.9 KB)  TX bytes:164443 (164.4 KB)
          Interrupt:25

eth1      Link encap:Ethernet  HWaddr 02:cd:56:eb:d5:f4  
          inet addr:10.1.1.2  Bcast:10.1.1.255  Mask:255.255.255.0
          inet6 addr: fe80::cd:56ff:feeb:d5 f4/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:94 errors:0 dropped:0 overruns:0 frame:0
          TX packets:51 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:4960 (4.9 KB)  TX bytes:4590 (4.5 KB)
          Interrupt:26

lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:16436  Metric:1
          RX packets:4 errors:0 dropped:0 overruns:0 frame:0
          TX packets:4 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:260 (260.0 B)  TX bytes:260 (260.0 B)
```


2)  Fill out the missing cells in the following table, using the results
of your individual experiment:

Node    | control interface name | control IP | data interface name | data IP
------- | ---------------------- | ---------- | ------------------- | --------
client  |                        |            |                     |
server  |                        |            |                     |

3)  Fill out the missing cells in the following table, using the results
of your individual experiment:

Link     |  Bandwidth (Mbits/sec)
-------- | ----------------------
control  |
data     |


4) What happens if you bring down the data interface of your node using the command

    sudo ifconfig DATAIFNAME down

 where `DATAIFNAME` is the name of the data interface (i.e. `eth0` or `eth1`)? What happens if you bring down the control interface of your node? Why?
